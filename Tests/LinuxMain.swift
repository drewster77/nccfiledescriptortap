import XCTest

import NCCFileDescriptorTapTests

var tests = [XCTestCaseEntry]()
tests += NCCFileDescriptorTapTests.allTests()
XCTMain(tests)
